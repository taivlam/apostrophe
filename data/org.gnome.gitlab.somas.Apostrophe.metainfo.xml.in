<?xml version="1.0" encoding="utf-8"?>
<component type="desktop-application">
  <id>@app-id@</id>
  <name>Apostrophe</name>
  <summary>Edit Markdown in style</summary>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0+</project_license>
  <developer_name>Manuel Genovés</developer_name>
  <developer id="es.escapist">
    <name>Manuel Genovés</name>
  </developer>
  <description>
    <p>Focus on your writing with a clean, distraction-free markdown editor.</p>
    <p>Features:</p>
    <ul>
      <li>An UI tailored to comfortable writing</li>
      <li>A distraction-free mode</li>
      <li>Dark, light and sepia themes</li>
      <li>Everything you expect from a text editor, such as spellchecking or document statistics</li>
      <li>Live preview of what you write</li>
      <li>Export to all kind of formats: PDF, Word/Libreoffice, LaTeX, or even HTML slideshows</li>
    </ul>
  </description>
  <launchable type="desktop-id">@app-id@.desktop</launchable>
  <url type="homepage">https://apps.gnome.org/Apostrophe/</url>
  <url type="bugtracker">https://gitlab.gnome.org/World/apostrophe/-/issues</url>
  <url type="help">https://gitlab.gnome.org/World/apostrophe/</url>
  <url type="donation">https://www.paypal.me/manuelgenoves</url>
  <url type="vcs-browser">https://gitlab.gnome.org/World/apostrophe/</url>
  <url type="translate">https://l10n.gnome.org/module/apostrophe/</url>
  <update_contact>manuel.genoves_at_gmail.com</update_contact>
  <translation type="gettext">@gettext-package@</translation>
  <screenshots>
    <screenshot type="default">
      <caption>Main window</caption>
      <image type="source">https://gitlab.gnome.org/World/apostrophe/-/raw/d50c630033213de5ee5cde0664e296b1fdcb5476/screenshots/main.png</image>
    </screenshot>
    <screenshot>
      <caption>Main window in dark mode</caption>
      <image type="source">https://gitlab.gnome.org/World/apostrophe/-/raw/d50c630033213de5ee5cde0664e296b1fdcb5476/screenshots/main-dark.png</image>
    </screenshot>
    <screenshot>
      <caption>Inserting a table with the help of the toolbar</caption>
      <image type="source">https://gitlab.gnome.org/World/apostrophe/-/raw/d50c630033213de5ee5cde0664e296b1fdcb5476/screenshots/table.png</image>
    </screenshot>
    <screenshot>
      <caption>The preview lets you see a live rendered version of your document</caption>
      <image type="source">https://gitlab.gnome.org/World/apostrophe/-/raw/d50c630033213de5ee5cde0664e296b1fdcb5476/screenshots/preview.png</image>
    </screenshot>
    <screenshot>
      <caption>The focus mode allow for a more distraction-free experience</caption>
      <image type="source">https://gitlab.gnome.org/World/apostrophe/-/raw/d50c630033213de5ee5cde0664e296b1fdcb5476/screenshots/focus.png</image>
    </screenshot>
  </screenshots>
  <branding>
    <color type="primary" scheme_preference="light">#90ded5</color>
    <color type="primary" scheme_preference="dark">#1b786d</color>
  </branding>
  <requires>
    <display_length compare="ge">1311</display_length>
  </requires>
  <recommends>
    <internet>always</internet>
  </recommends>
  <supports>
    <control>pointing</control>
    <control>keyboard</control>
    <control>touch</control>
  </supports>
  <releases>
    <release type="stable" version="3.2" date="2024-09-25T00:00:00Z">
      <description>
        <p>This release fixes the following bugs:</p>
        <ul>
          <li>Fixed disability flag not being shown in the savebar</li>
          <li>Fixed bold text not working on the live preview</li>
        </ul>
        <p>This release adds the following features:</p>
        <ul>
          <li>Updated dependencies</li>
          <li>Updated Basque, Norwegian, Spanish, Turkish, Romanian and Finish translations</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="3.1" date="2024-06-28T00:00:00Z">
      <description>
        <p>This release fixes the following bugs:</p>
        <ul>
          <li>Fixed buggy drag and drop behavior</li>
          <li>Fixed labels not displaying in the Recents popover when they had an ampersand</li>
        </ul>
        <p>This release adds the following feature:</p>
        <ul>
          <li>Added pride colors to the savebar</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="3.0" date="2024-05-01T00:00:00Z">
      <description>
        <p>This release adds the following features:</p>
        <ul>
          <li>Port to GTK4 and the newest libadwaita widgets</li>
          <li>Improved UI and styling</li>
          <li>New toolbar for interactively formatting text</li>
          <li>Implement autoindentation and brace autocompletion</li>
          <li>Live stats for selected text</li>
          <li>Improved Hemingway Mode</li>
          <li>New secured preview mode when opening potentially dangerous files</li>
          <li>Detect when the file being edited is changed by another program</li>
          <li>Added DocuWiki to the available export formats</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.6.3" date="2022-04-29T00:00:00Z">
      <description>
        <p>This release fixes the following bugs:</p>
        <ul>
          <li>Fix a bug where the preview wouldn't be shown when opening a file from Files or the CLI</li>
          <li>Fix a bug where horizontal preview wouldn't request enough space at startup</li>
          <li>Removed swipe gestures from the preview panel</li>
        </ul>
        <p>This release adds the following feature:</p>
        <ul>
          <li>Added Persian translations, updated Catalan, German , Finnish and Turkish ones</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.6.2" date="2022-03-31T00:00:00Z">
      <description>
        <p>This release fixes the following bug:</p>
        <ul>
          <li>Fix handling of windowed preview mode on some circumstances</li>
        </ul>
        <p>This release adds the following features:</p>
        <ul>
          <li>Add proper titles to the preview window</li>
          <li>Update translations</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.6.1" date="2022-03-20T00:00:00Z">
      <description>
        <p>This release fixes the following bug:</p>
        <ul>
          <li>A small bug in 2.6 which didn't allow to save files</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.6" date="2022-03-19T00:00:00Z">
      <description>
        <p>This release adds the following features:</p>
        <ul>
          <li>Implemented multiwindow support</li>
          <li>Save window state on exit</li>
          <li>Ported to libhandy</li>
          <li>Improved the overall UI</li>
          <li>New animations</li>
          <li>Follow the Dark Mode global preference</li>
          <li>Add an "automatic" color mode</li>
          <li>Improve the High Contrast styles</li>
          <li>Improve inline preview markup</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.5" date="2021-08-28T00:00:00Z">
      <description>
        <p>This release adds the following features:</p>
        <ul>
          <li>Added Sepia Mode</li>
          <li>Improved exported HTML on small screens</li>
          <li>Updated metadata and runtime version</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.4" date="2021-03-10T00:00:00Z">
      <description>
        <p>This release fixes the following bugs:</p>
        <ul>
          <li>Fixed a bug where the cursor could go over the bottom of the window</li>
          <li>Fixed a bug where in certain circumstances the bottom line couldn't be clicked</li>
          <li>Improved behaviour when dealing with unsaved files.</li>
          <li>Improved start/end sentence detection in Focus Mode</li>
          <li>Small improvements in the headerbar title</li>
          <li>Cleaned a little bit the Shortcuts Window</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.3" date="2021-03-01T00:00:00Z">
      <description>
        <p>This release adds the following features:</p>
        <ul>
          <li>New export dialog</li>
          <li>Improved Reveal.js slideshow exports</li>
          <li>Better input/output handling</li>
          <li>Inhibit logout if the document is not saved</li>
          <li>Migrate to xelatex</li>
          <li>Better internal path handling</li>
          <li>New animated progressbar on saving</li>
          <li>Strict sandboxing support</li>
        </ul>
        <p>This release fixes the following bugs:</p>
        <ul>
          <li>Undo/redo improvements</li>
          <li>Fixes for bad markdown parsing of unordered lists</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.2.0.3" date="2020-05-04T00:00:00Z">
      <description>
        <p>This release fixes the following bugs:</p>
        <ul>
          <li>Better wording for some areas</li>
          <li>Fixed some issues where Apostrophe couldn't open files from CLI/context menu</li>
          <li>Now Apostrophe checks if there is unsaved work before exiting via CTRL + Q/W</li>
          <li>Better parsing of weblinks and links to files</li>
          <li>Fixed a bug where the bottom portion of the screen was insensitive to mouse input</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.2.0.2" date="2020-04-20T00:00:00Z">
      <description>
        <p>This release fixes the following bugs:</p>
        <ul>
          <li>Fixes for the Elementary OS platform</li>
          <li>Fixed some links and metadata</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.2.0.1" date="2020-04-18T00:00:00Z">
      <description>
        <p>This release adds the following features:</p>
        <ul>
          <li>New headerbar design</li>
          <li>New preview modes, with the option to sync them to the edit view</li>
          <li>New preview mode selector</li>
          <li>New theme selector</li>
          <li>Rework the autohiding mechanism; now the headerbar fades away when typing, only to reappear when the cursor moves to the top portion of the window</li>
          <li>Now the content of the texview goes visually bellow the headerbar</li>
          <li>Overall better styling</li>
          <li>Added Hemingway mode, which disables the backspace key</li>
          <li>Added Github Flavoured Markdow, MultiMarkdown, Pandoc's Markdown and Commonmark support, being CommonMark the default from now on</li>
          <li>New stats counter, with the option to show count of characters/words/sentences/paragrafs/reading time</li>
          <li>Better handling of DnD events</li>
          <li>Export to A4 by default</li>
          <li>Technical improvements   * Port of the buildsystem to Meson. Now you can hit the "build" button on Builder and everything works as expected  * Port to gspell  * Partial port to gresources  * Overall refactorization of the codebase  * General bugfixes and improvements</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.1.5" date="2019-03-10T00:00:00Z">
      <description>
        <p>This release adds the following features:</p>
        <ul>
          <li>Added italian language</li>
          <li>Initial themes support: now apostrophe adapts his colors to the current GTK theme</li>
          <li>Disabled scroll gradient, can be enabled in the preferences dialog</li>
          <li>Allow to disable headerbar autohidding in Dconf</li>
          <li>Now a single click is enough to open files in the recent files popover</li>
          <li>Spellchecking status is now saved between sessions</li>
          <li>Minor UI fixes</li>
          <li>Added -d flag to enable webdev tools</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.1.4" date="2018-12-06T00:00:00Z">
      <description>
        <p>This release adds the following feature:</p>
        <ul>
          <li>Updated css styles.</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.1.3" date="2018-11-28T00:00:00Z">
      <description>
        <p>This release adds the following feature:</p>
        <ul>
          <li>This release features a new logo, polishes the Appmenu, fixes usability bugs and flatpak related bugs.</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.1.2" date="2018-07-27T00:00:00Z">
      <description>
        <p>This release adds the following feature:</p>
        <ul>
          <li>This release provides a fix to a bug that caused Apostrophe to not mark properly **bold**, *cursive*, and ***bold and cursive*** words.</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.1.1" date="2018-07-26T00:00:00Z">
      <description>
        <p>This release fixes the following bugs:</p>
        <ul>
          <li>One on focus mode which caused the lines to be highlighted on edit rather than on click</li>
          <li>Non symbolic icons on the searchbar</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.1.0" date="2018-07-18T00:00:00Z">
      <description>
        <p>This release adds the following features:</p>
        <ul>
          <li>Drop AppMenu support</li>
          <li>HeaderBar and menus redesign, with a new unified menu and quick access buttons on the headerbar</li>
          <li>Now the fullscreen view shows a headerbar when the cursor approaches the top of the screen</li>
          <li>A new unified export dialog, with updated options, and quick access to pdf, odt and html export</li>
          <li>Bugfixes.</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.0.4" date="2018-06-24T00:00:00Z">
      <description>
        <p>This release adds the following features:</p>
        <ul>
          <li>Now the menu is a Popover instead a regular menu.</li>
          <li>The headerbar matches the theme selected for the application.</li>
          <li>Updated translations.</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.0.3" date="2018-06-14T00:00:00Z">
      <description>
        <p>This release fixes the following bug:</p>
        <ul>
          <li>Small bug fixes, updated links.</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.0.2" date="2018-05-16T00:00:00Z">
      <description>
        <p>This release fixes the following bug:</p>
        <ul>
          <li>Fix a bug with the preview mode.</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.0.1" date="2018-05-14T00:00:00Z">
      <description>
        <p>This release fixes the following bug:</p>
        <ul>
          <li>Don’t use env variable to check if in flatpak.</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="2.0.0" date="2018-05-12T00:00:00Z">
      <description>
        <p>This release adds the following features:</p>
        <ul>
          <li>First re-release</li>
        </ul>
      </description>
    </release>
  </releases>
  <content_rating type="oars-1.1"/>
</component>
